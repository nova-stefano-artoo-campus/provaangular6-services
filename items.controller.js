angular.module('app').controller('ItemsCtrl', function($scope, ItemsSrv) {

    $scope.listaArmi = ItemsSrv.getArmi();

    $scope.addArma = function() {
      ItemsSrv.addArma();
    };

    $scope.$watch("nome", function() {
      $scope.armaTrovata = ItemsSrv.cercaArma($scope.nome);
      console.log($scope.armaTrovata);
    });

    $scope.eliminaArma = function(indice) {
      ItemsSrv.eliminaArma(indice);
    };
});
